//
//  ARUIApp.swift
//  ARUI
//
//  Created by Braian Gutierrez on 9/2/22.
//

import SwiftUI

@main
struct ARUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
